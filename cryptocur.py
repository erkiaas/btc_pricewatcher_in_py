# -*- coding: utf-8 -*-
"""
This is a program, which determines the highest rise and drop of BTC/USD compared to the previous day
and also the greates BTC price in USD of all time, 
all this using blockchain.com's data.
"""
from collections import namedtuple
import json
from urllib import urlopen
import datetime

url = urlopen('https://api.blockchain.info/charts/market-price?timespan=all&format=json')
result = json.loads(url.read())  

Point = namedtuple('Point', ['day', 'value'])

values = []
days = []

for i in range(len(result['values'])):
    values.append(result['values'][i][u'y'])
    days.append(result['values'][i][u'x'])


raw_data = list(zip(days, values))
data = [Point(day=day, value=value) for day, value in raw_data]

def normaldate(date):
    date = datetime.datetime.fromtimestamp(date).strftime('%d %b %Y')
    return date
 

def increase():
    biggest_change = 0
    day_of_biggest_change = None
    for i in range(len(data) - 1):
        previous = data[i].value
        current = data[i+1].value
        change = current - previous
        if change > biggest_change:
            biggest_change = change
            day_of_biggest_change = data[i+1].day
    return [round(biggest_change, 2), day_of_biggest_change]


def decrease():
    biggest_change = 0
    day_of_biggest_change = None
    for i in range(len(data) - 1):
        previous = data[i].value
        current = data[i+1].value
        change = current - previous
        if change < biggest_change:
            biggest_change = change
            day_of_biggest_change = data[i+1].day
    return [round(biggest_change, 2), day_of_biggest_change]

def greatest():
    biggest = 0
    day_of_biggest = None
    for i in range(len(data) - 1):
        current = data[i].value
        if current > biggest:
            biggest = current
            day_of_biggest = data[i+1].day  
    return[round(biggest, 2), day_of_biggest]
          

def output():
    print("The greatest increase of price was " + str(increase()[0]) + "$ on " + str(normaldate(increase()[1])))
    print("The greatest decrease of price was " + str(decrease()[0]) + "$ on " + str(normaldate(decrease()[1])))
    print("The greatest price was " + str(greatest()[0]) + "$ on " + str(normaldate(greatest()[1])))


""""
Here starts the unittest part
"""

import unittest

class TheTest(unittest.TestCase):
    def testOne(self):
        self.assertEqual(increase(), [4623.54, 1512604800])
    def testTwo(self):
        self.assertEqual(decrease(), [-2053.29, 1515369600])
    def testThree(self):
        self.assertEqual(greatest(), [19289.78, 1513641600])


def main():
    output()
    unittest.main()

if __name__ == '__main__':
    main()
        