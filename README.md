# cryptocurrency_pricewatcher_in_py
This is a program, which determines the highest raise of price of the Bitcoin (BTC),
compared to the previous day, highest drop of price of the BTC, and the all time peak
price of the BTC.

All the data is grabbed from the blockchain.com's api in real-time, and the output is
displayed on the console screen.

There are also unit tests, ensureing the program gives right answers. Those are written
on 10th January 2018, and therefore are subject to change as the market fluctuates. 
